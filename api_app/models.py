from api_app.fields import jsonStrField

from django.db import models


class ProtfolioData(models.Model):
    jsonStr = jsonStrField(blank=False)
    time = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=False)
    label = models.CharField(max_length=32, blank=True)

    def save(self, *args, **kwargs):
        if self.active:
            qs = type(self).objects.filter(active=True)
            if self.pk:
                qs = qs.exclude(pk=self.pk)
            qs.update(active=False)
        super(ProtfolioData, self).save(*args, **kwargs)

    def __str__(self):
        time_str = self.time.strftime("%m/%d/%Y, %H:%M:%S")
        active_str = (' - ACTIVE' if self.active else '')
        label_str = (' (' + self.label.lower() + ')' if self.label else '')
        return "prof_data-" + time_str + label_str + active_str


class ResumeData(models.Model):
    jsonStr = jsonStrField(blank=False)
    time = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=False)
    label = models.CharField(max_length=32, blank=True)

    def save(self, *args, **kwargs):
        if self.active:
            qs = type(self).objects.filter(active=True)
            if self.pk:
                qs = qs.exclude(pk=self.pk)
            qs.update(active=False)
        super(ResumeData, self).save(*args, **kwargs)

    def __str__(self):
        time_str = self.time.strftime("%m/%d/%Y, %H:%M:%S")
        active_str = (' - ACTIVE' if self.active else '')
        label_str = (' (' + self.label.lower() + ')' if self.label else '')
        return "res_data-" + time_str + label_str + active_str

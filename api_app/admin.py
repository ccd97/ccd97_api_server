from api_app import models
from api_app.fields import jsonStrField

from django.contrib import admin

from django_ace import AceWidget


class JsonStrModel(admin.ModelAdmin):
    formfield_overrides = {
        jsonStrField: {'widget': AceWidget(
                mode='json', theme='github',
                width='50vw', height='50vh',
                wordwrap=True, showprintmargin=False,
                tabsize=4)
            }
    }


admin.site.register(models.ProtfolioData, JsonStrModel)
admin.site.register(models.ResumeData, JsonStrModel)

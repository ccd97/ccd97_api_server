import json

from django.core.exceptions import ValidationError
from django.db import models


class jsonStrField(models.TextField):

    def __init__(self, *args, **kwargs):
        self.default_validators = [self.validate_jsonstr]
        super(jsonStrField, self).__init__(*args, **kwargs)

    def validate_jsonstr(self, jsonStr):
        try:
            json.loads(jsonStr)
        except ValueError as e:
            raise ValidationError(str(e))

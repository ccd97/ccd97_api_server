from api_app import data_handler

from django.http import Http404, JsonResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def protfolioDataView(request):
    try:
        return JsonResponse(data_handler.getProtfolioData())
    except Exception:
        raise Http404("Invalid or no protfolio data")


@csrf_exempt
def resumeDataView(request):
    try:
        return JsonResponse(data_handler.getResumeData())
    except Exception:
        raise Http404("Invalid or no resume data")

import json

from api_app import models


def getProtfolioData():
    try:
        obj = models.ProtfolioData.objects.filter(active=True)
        if not obj.exists():
            obj = models.ProtfolioData.objects.latest('time')
        else:
            obj = obj.first()
        json_obj = json.loads(obj.jsonStr)
        return json_obj
    except Exception:
        raise Exception


def getResumeData():
    try:
        obj = models.ResumeData.objects.filter(active=True)
        if not obj.exists():
            obj = models.ResumeData.objects.latest('time')
        else:
            obj = obj.first()
        json_obj = json.loads(obj.jsonStr)
        return json_obj
    except Exception:
        raise Exception
